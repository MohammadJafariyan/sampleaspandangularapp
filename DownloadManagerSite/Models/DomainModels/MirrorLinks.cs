//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DownloadManagerSite.Models.DomainModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class MirrorLinks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MirrorLinks()
        {
            this.MirrorLinkLogs = new HashSet<MirrorLinkLogs>();
        }
    
        public int Id { get; set; }
        public int MainLinkId { get; set; }
        public int ServerId { get; set; }
        public string TargetLink { get; set; }
        public int RequestCount { get; set; }
        public System.DateTime RegDate { get; set; }
        public Nullable<System.DateTime> LastAccessDate { get; set; }
        public byte Status { get; set; }
    
        public virtual MainLinks MainLinks { get; set; }
        public virtual Servers Servers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MirrorLinkLogs> MirrorLinkLogs { get; set; }
    }
}
