﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DownloadManagerSite.Models.DomainModels
{
    public partial class DbDownloadManagerSite : DbContext
    {
        public DbDownloadManagerSite()
            : base("DbDownloadManagerSite")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<MainLinks>()
                .HasMany(e => e.MirrorLinks)
                .WithRequired(e => e.MainLinks)
                .HasForeignKey(e=>e.MainLinkId);
            
            modelBuilder.Entity<MirrorLinks>()
                .HasMany(e => e.MirrorLinkLogs)
                .WithRequired(e => e.MirrorLinks)
                .HasForeignKey(e=>e.MirrorLinkId);
            
            
            modelBuilder.Entity<Servers>()
                .HasMany(e => e.MirrorLinks)
                .WithRequired(e => e.Servers)
                .HasForeignKey(e=>e.ServerId);
        }

        public virtual DbSet<MainLinks> MainLinks { get; set; }
        public virtual DbSet<MirrorLinks> MirrorLinks { get; set; }
        public virtual DbSet<Servers> Servers { get; set; }
        public virtual DbSet<MirrorLinkLogs> MirrorLinkLogs { get; set; }
    }
}