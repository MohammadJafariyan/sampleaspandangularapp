
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/08/2020 13:26:13
-- Generated from EDMX file: E:\workplace\git\1399\xordad\DownloadManagerSite\DownloadManagerSite\Models\DomainModels\DbDownloadManagerSite.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [telegrambotDownloadManager];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_MirrorLinkLogs_MirrorLinks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MirrorLinkLogs] DROP CONSTRAINT [FK_MirrorLinkLogs_MirrorLinks];
GO
IF OBJECT_ID(N'[dbo].[FK_MirrorLinks_MainLinks]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MirrorLinks] DROP CONSTRAINT [FK_MirrorLinks_MainLinks];
GO
IF OBJECT_ID(N'[dbo].[FK_MirrorLinks_Servers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MirrorLinks] DROP CONSTRAINT [FK_MirrorLinks_Servers];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[MainLinks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MainLinks];
GO
IF OBJECT_ID(N'[dbo].[MirrorLinkLogs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MirrorLinkLogs];
GO
IF OBJECT_ID(N'[dbo].[MirrorLinks]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MirrorLinks];
GO
IF OBJECT_ID(N'[dbo].[Servers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Servers];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'MainLinks'
CREATE TABLE [dbo].[MainLinks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SourceLink] nvarchar(400)  NOT NULL,
    [RequestCount] int  NOT NULL,
    [RegDate] datetime  NOT NULL,
    [LastAccessDate] datetime  NULL,
    [Status] tinyint  NOT NULL
);
GO

-- Creating table 'MirrorLinks'
CREATE TABLE [dbo].[MirrorLinks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MainLinkId] int  NOT NULL,
    [ServerId] int  NOT NULL,
    [TargetLink] nvarchar(300)  NULL,
    [RequestCount] int  NOT NULL,
    [RegDate] datetime  NOT NULL,
    [LastAccessDate] datetime  NULL,
    [Status] tinyint  NOT NULL
);
GO

-- Creating table 'Servers'
CREATE TABLE [dbo].[Servers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [IpAddress] nvarchar(15)  NOT NULL,
    [ServerName] nvarchar(100)  NULL,
    [Status] tinyint  NOT NULL
);
GO

-- Creating table 'MirrorLinkLogs'
CREATE TABLE [dbo].[MirrorLinkLogs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [MirrorLinkId] int  NOT NULL,
    [TimeOut] int  NOT NULL,
    [RegDate] datetime  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'MainLinks'
ALTER TABLE [dbo].[MainLinks]
ADD CONSTRAINT [PK_MainLinks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MirrorLinks'
ALTER TABLE [dbo].[MirrorLinks]
ADD CONSTRAINT [PK_MirrorLinks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Servers'
ALTER TABLE [dbo].[Servers]
ADD CONSTRAINT [PK_Servers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MirrorLinkLogs'
ALTER TABLE [dbo].[MirrorLinkLogs]
ADD CONSTRAINT [PK_MirrorLinkLogs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [MainLinkId] in table 'MirrorLinks'
ALTER TABLE [dbo].[MirrorLinks]
ADD CONSTRAINT [FK_MirrorLinks_MainLinks]
    FOREIGN KEY ([MainLinkId])
    REFERENCES [dbo].[MainLinks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MirrorLinks_MainLinks'
CREATE INDEX [IX_FK_MirrorLinks_MainLinks]
ON [dbo].[MirrorLinks]
    ([MainLinkId]);
GO

-- Creating foreign key on [ServerId] in table 'MirrorLinks'
ALTER TABLE [dbo].[MirrorLinks]
ADD CONSTRAINT [FK_MirrorLinks_Servers]
    FOREIGN KEY ([ServerId])
    REFERENCES [dbo].[Servers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MirrorLinks_Servers'
CREATE INDEX [IX_FK_MirrorLinks_Servers]
ON [dbo].[MirrorLinks]
    ([ServerId]);
GO

-- Creating foreign key on [MirrorLinkId] in table 'MirrorLinkLogs'
ALTER TABLE [dbo].[MirrorLinkLogs]
ADD CONSTRAINT [FK_MirrorLinkLogs_MirrorLinks]
    FOREIGN KEY ([MirrorLinkId])
    REFERENCES [dbo].[MirrorLinks]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MirrorLinkLogs_MirrorLinks'
CREATE INDEX [IX_FK_MirrorLinkLogs_MirrorLinks]
ON [dbo].[MirrorLinkLogs]
    ([MirrorLinkId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------