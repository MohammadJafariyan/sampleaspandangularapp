﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DownloadManagerSite.Models.Enums
{
    public enum Status
    {
        inactive = 0,
        active = 1
    }
}