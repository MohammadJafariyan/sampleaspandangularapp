﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DownloadManagerSite.Models.DomainModels
{
    public class ServerMetadata
    {
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int Id { get; set; }

        [DisplayName("آدرس Ip")]
        [Display(Name = "آدرس Ip")]
        [Required(ErrorMessage = "آدرس آی‌پی را وارد کنید")]
        [StringLength(15, ErrorMessage = "آدرس آی‌پی حداکثر 15 کاراکتر می باشد")]
        [MinLength(7, ErrorMessage = "آدرس آی‌پی حداقل 7 کاراکتر می باشد")]
        [RegularExpression((@"\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b"), ErrorMessage = "آدرس آی‌پی را به درستی وارد کنید")]
        public string IpAddress { get; set; }

        [DisplayName("نام سرور")]
        [Display(Name = "نام سرور")]
        [Required(ErrorMessage = "نام سرور را وارد کنید")]
        [MaxLength(100, ErrorMessage ="نام سرور حداکثر 100 کاراکتر می تواند باشد")]
        public string ServerName { get; set; }

        [DisplayName("وضعیت سرور")]
        [Display(Name = "وضعیت سرور")]
        public Nullable<byte> Status { get; set; }
    }

    [MetadataType(typeof(ServerMetadata))]
    public partial class Servers
    {
    }
}