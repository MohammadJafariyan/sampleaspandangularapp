﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DownloadManagerSite.Models.DomainModels
{
    public class MainLinkMetadata
    {
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int Id { get; set; }
        
        [DisplayName("لینک مبدا")]
        [Display(Name = "لینک مبدا")]
        [Required(ErrorMessage = "لینک مبدا را وارد کنید")]
        [MaxLength(400, ErrorMessage = "حداکثر طول لینک مبدا 400 کاراکتر می باشد")]
        public string SourceLink { get; set; }

        [DisplayName("تعداد درخواست")]
        [Display(Name = "تعداد درخواست")]
        public int RequestCount { get; set; }

        [DisplayName("تاریخ ثبت")]
        [Display(Name = "تاریخ ثبت")]
        public System.DateTime RegDate { get; set; }

        [DisplayName("آخرین تاریخ دسترسی")]
        [Display(Name = "آخرین تاریخ دسترسی")]
        public Nullable<System.DateTime> LastAccessDate { get; set; }

        [DisplayName("وضعیت")]
        [Display(Name = "وضعیت")]
        public Nullable<byte> Status { get; set; }
    }

    [MetadataType(typeof(MainLinkMetadata))]
    public partial class MainLinks
    {
    }
}