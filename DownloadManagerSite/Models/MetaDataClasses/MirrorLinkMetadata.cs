﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DownloadManagerSite.Models.DomainModels
{
    public class MirrorLinkMetadata
    {
        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int Id { get; set; }

        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int MainLinkId { get; set; }

        [ScaffoldColumn(false)]
        [Bindable(false)]
        public int ServerId { get; set; }

        [DisplayName("لینک مقصد")]
        [Display(Name = "لینک مقصد")]
        [MaxLength(300, ErrorMessage =("حداکثر تعداد کاراکترهای لینک مقصد 300 کاراکتر می باشد"))]
        [Required(ErrorMessage = "لینک مقصد را وارد کنید")]
        public string TargetLink { get; set; }

        [DisplayName("تعداد درخواست")]
        [Display(Name = "تعداد درخواست")]
        public int RequestCount { get; set; }

        [DisplayName("تاریخ ثبت")]
        [Display(Name = "تاریخ ثبت")]
        public System.DateTime RegDate { get; set; }

        [DisplayName("آخرین تاریخ دسترسی")]
        [Display(Name = "آخرین تاریخ دسترسی")]
        public Nullable<System.DateTime> LastAccessDate { get; set; }

        [DisplayName("وضعیت")]
        [Display(Name = "وضعیت")]
        public Nullable<byte> Status { get; set; }
    }

    [MetadataType(typeof(MirrorLinkMetadata))]
    public partial class MirrorLinks
    {
    }
}