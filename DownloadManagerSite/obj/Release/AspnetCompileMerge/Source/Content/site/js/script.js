﻿

//**************************************Register

function validateFormRegister(formId) {
    
    form = document.getElementById(formId);
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].value === '' && form.elements[i].hasAttribute('required')) {
            myAlert("danger", "خطا", "ورود اطلاعات همه فیلدها اجباری است");
            return false;
        }
    }

    //if (checkEmail('email') == false) {
    //    myAlert("danger", "خطا", "فرمت ایمیل وارد شده صحیح نمی باشد");
    //    return;
    //}

    if (checkMobile('mobile') == false) {
        myAlert("danger", "خطا", "شماره موبایل وارد شده اشتباه می باشد");
        return;
    }

    if (checkPassword('password1', 'password2') == false) {
        myAlert("danger", "خطا", "رمز عبورهای وارد شده یکسان نمی باشند");
        return;
    }

    register(form);
}
function image_option(elem) {
    $(".image-option").removeClass("image-option-active");
    $(elem).addClass("image-option-active");
    $(elem).find("input[type='radio']").prop("checked", true);
    //var parentElement =  $(elem)[0];

}
