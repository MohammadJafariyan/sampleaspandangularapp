﻿using System.Collections.Generic;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace DownloadManagerSite.Areas.Sample.Models
{
    public class Retailer:Entity
    {
        public List<Customer> Customers { get; set; }
        public string Name { get; set; }
    }
}