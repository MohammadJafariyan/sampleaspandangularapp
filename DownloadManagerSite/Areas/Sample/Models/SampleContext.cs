﻿using System.Data.Entity;
using DownloadManagerSite.Migrations;
using Engine.SysAdmin.Service;

namespace DownloadManagerSite.Areas.Sample.Models
{
    public class SampleContext:MyContextBase
    {
        public SampleContext() : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SampleContext,Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Retailer>().HasMany(c => c.Customers)
                .WithRequired(r => r.Retailer).HasForeignKey(r => r.RetailerId);
        }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<Retailer> Retailer { get; set; }
        
        
    }
}