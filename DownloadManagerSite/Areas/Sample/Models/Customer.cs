﻿using System.Collections.Generic;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace DownloadManagerSite.Areas.Sample.Models
{
    public class Customer:Entity
    {
        public string Name { get; set; }
        
        public Retailer Retailer { get; set; }
        public int RetailerId { get; set; }
    }
}