﻿using System.Web.Http;
using DownloadManagerSite.Areas.Sample.Models;
using DownloadManagerSite.Areas.Sample.Service;
using DownloadManagerSite.Areas.sysadmin.Controllers;
using TelegramBotsWebApplication.ActionFilters;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace DownloadManagerSite.Areas.Sample.Controllers
{
    
    [WebApiCurrentRequstSetterFilter]
    public class BaseCustomerApiController:GenericApiController<Customer>
    {
        public BaseCustomerApiController(BaseCustomerService service) : base(service)
        {
        }
    }
}