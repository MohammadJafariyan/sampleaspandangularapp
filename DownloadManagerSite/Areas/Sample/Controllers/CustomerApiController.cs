﻿using DownloadManagerSite.Areas.Sample.Models;
using DownloadManagerSite.Areas.Sample.Service;
using TelegramBotsWebApplication.ActionFilters;
using TelegramBotsWebApplication.Areas.Admin.Models;

namespace DownloadManagerSite.Areas.Sample.Controllers
{
    [WebApiCurrentRequstSetterFilter]
    public class CustomerApiController : BaseCustomerApiController
    {
        public CustomerApiController(CustomerService customerService) : base(customerService)
        {
        }

        public override MyDataTableResponse<Customer> GetAll()
        {
            return null;
        }

        public override MyEntityResponse<int> Save(Customer entity)
        {
            return null;
        }

        public override MyEntityResponse<int> DeleteById(int id)
        {
            return null;
        }
    }
}