﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using DownloadManagerSite.Areas.Sample.Models;
using DownloadManagerSite.Areas.Sample.Service;
using DownloadManagerSite.Areas.sysadmin.Controllers;
using SignalRMVCChat.DependencyInjection;
using SignalRMVCChat.SysAdmin.Service;
using TelegramBotsWebApplication;
using TelegramBotsWebApplication.Areas.Admin.Models;
using TelegramBotsWebApplication.Areas.Admin.Service;
using TelegramBotsWebApplication.Service;

namespace DownloadManagerSite.Areas.Sample.Controllers
{
    public class SecurityApiController : ApiController
    {
        private readonly RetailerService _retailerService;
        private readonly CustomerService _customerService;
        

        public SecurityApiController()
        {
            _retailerService = Injector.Inject<RetailerService>();
            _customerService = Injector.Inject<CustomerService>();
        }

        public MyEntityResponse<string> Login(string username, string password)
        {
            if (username == "customer" && password == "customer")
            {
                return MakeToken();
            }

            if (username == "retailer" && password == "retailer")
            {
                return MakeTokenRetailer();
            }
            
            return new MyEntityResponse<string>
            {
                Single = null,
                Status = MyResponseStatus.Fail,
                Message = "نام کاربری یا رمز عبور صحیح نیست"
            };
        }

        private MyEntityResponse<string> MakeTokenRetailer()
        {
            return TokenHelper(_retailerService, "Retailer");
        }

        private MyEntityResponse<string> TokenHelper<T>(GenericService<T>  service, string name) where T:Entity
        {
            var Retailer = service.GetQuery().First();
            string token = MySpecificGlobal.MakeToken(Retailer.Id, name);
            return new MyEntityResponse<string>
            {
                Single = token
            };
        }

        private MyEntityResponse<string> MakeToken()
        {
            return TokenHelper(_customerService, "Customer");
        }
    }

    public class MySpecificGlobal
    {
        public static string MakeToken(int id, string name)
        {
            return EncryptionHelper.Encrypt($@"{name}_{id}");
        }

        public static CurrentRequestHolder ParseToken(string token, CurrentRequestHolder currentRequestHolder)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new Exception("دسترسی ندارید توکن خالی است");
            }

            var Decrypt = EncryptionHelper.Decrypt(token);
            string name = Decrypt.Split('_')[0];
            string id = Decrypt.Split('_')[1];

            if (name == "Customer")
            {
                currentRequestHolder.CustomerId = int.Parse(id);
                currentRequestHolder.UserType = UserType.Customer;
            }
            else if (name == "Retailer")
            {
                currentRequestHolder.RetailerId = int.Parse(id);
                currentRequestHolder.UserType = UserType.Retailer;
            }
            else
            {
                throw new Exception("دسترسی ندارید توکن شناسایی نشد");
            }

            return currentRequestHolder;
        }
    }
}