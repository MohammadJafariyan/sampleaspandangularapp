﻿using DownloadManagerSite.Areas.Sample.Models;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace DownloadManagerSite.Areas.Sample.Service
{
    public class RetailerService:GenericService<Retailer>
    {
        public RetailerService() : base(null)
        {
        }
    }
}