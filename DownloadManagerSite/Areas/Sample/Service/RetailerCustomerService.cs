﻿using System;
using System.Linq;
using DownloadManagerSite.Areas.Sample.Models;
using SignalRMVCChat.SysAdmin.Service;

namespace DownloadManagerSite.Areas.Sample.Service
{
    public class RetailerCustomerService:BaseCustomerService
    {
        private readonly RetailerService _retailerService;

        public RetailerCustomerService(CurrentRequestHolder currentRequestHolder,
            RetailerService retailerService) : base(currentRequestHolder)
        {
            _retailerService = retailerService;
        }

        protected override void CheckAccess()
        {
            if (CurrentRequestHolder.RetailerId.HasValue==false)
            {
                throw new Exception("دسترسی ندارید");
            }

            //اگر نداشته باشد حتما خطا خواهد داد
            _retailerService.GetById(CurrentRequestHolder.RetailerId.Value);
        }


        /// <summary>
        /// تمامی متد ها از این متد استفاده می کنند و اگر این فیلتر شود برای کل متد ها فیلتر خواهد شد
        /// </summary>
        /// <returns></returns>
        public override IQueryable<Customer> GetQuery()
        {
            return base.GetQuery().Where(q=>q.RetailerId==CurrentRequestHolder.RetailerId);
        }

       
    }
}