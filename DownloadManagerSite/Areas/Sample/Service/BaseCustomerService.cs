﻿using DownloadManagerSite.Areas.Sample.Models;
using SignalRMVCChat.SysAdmin.Service;
using TelegramBotsWebApplication.Areas.Admin.Service;

namespace DownloadManagerSite.Areas.Sample.Service
{
    public class BaseCustomerService:AccessProviderGenericService<Customer>
    {
        protected readonly CurrentRequestHolder CurrentRequestHolder;

        public BaseCustomerService(CurrentRequestHolder currentRequestHolder) : base(null)
        {
            CurrentRequestHolder = currentRequestHolder;
        }

    }
    
    
}