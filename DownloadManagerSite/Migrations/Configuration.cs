﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using DownloadManagerSite.Areas.Sample.Models;

namespace DownloadManagerSite.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<SampleContext>
    {
        
        
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(SampleContext context)
        {
            string[] names=
            {
                "محمد ","علی","شهرزادر","ایرانی","عباس", "قاسم","کورش","مسعود"   
            };

            List<string> _names = new List<string>();
            for (int i = 0; i < 5; i++)
            {
               int one= new Random().Next(0, names.Length - 1);
              int two=  new Random().Next(0, names.Length - 1);
              
              _names.Add(names[one]+" "+ names[two]+ "ای");
            }

            
            int one1= new Random().Next(0, names.Length - 1);
            int two1=  new Random().Next(0, names.Length - 1);


            var retailer = new Retailer
            {
                Name = names[one1] + " " + names[two1] + "ای"
            };
            context.Retailer.Add(retailer);

            context.SaveChanges();
            
            for (int i = 0; i < _names.Count; i++)
            {
                var Customer = new Customer
                {
                    Name = _names[i],
                    RetailerId = retailer.Id
                };

                context.Customers.Add(Customer);
            }

            context.SaveChanges();
            
            base.Seed(context);
        }
    }

}
