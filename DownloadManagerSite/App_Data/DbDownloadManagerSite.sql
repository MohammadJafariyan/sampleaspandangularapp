USE [master]
GO
/****** Object:  Database [DbDownloadManagerSite]    Script Date: 02/05/2020 00:36:39 ******/
CREATE DATABASE [DbDownloadManagerSite]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DbDownloadManagerSite', FILENAME = N'D:\MyProject\DownloadManagerSite\DownloadManagerSite\DownloadManagerSite\App_Data\DbDownloadManagerSite.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DbDownloadManagerSite_log', FILENAME = N'D:\MyProject\DownloadManagerSite\DownloadManagerSite\DownloadManagerSite\App_Data\DbDownloadManagerSite_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DbDownloadManagerSite] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DbDownloadManagerSite].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DbDownloadManagerSite] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET ARITHABORT OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DbDownloadManagerSite] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DbDownloadManagerSite] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DbDownloadManagerSite] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DbDownloadManagerSite] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET RECOVERY FULL 
GO
ALTER DATABASE [DbDownloadManagerSite] SET  MULTI_USER 
GO
ALTER DATABASE [DbDownloadManagerSite] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DbDownloadManagerSite] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DbDownloadManagerSite] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DbDownloadManagerSite] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DbDownloadManagerSite] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DbDownloadManagerSite', N'ON'
GO
USE [DbDownloadManagerSite]
GO
/****** Object:  User [saa]    Script Date: 02/05/2020 00:36:39 ******/
CREATE USER [saa] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[MainLinks]    Script Date: 02/05/2020 00:36:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MainLinks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SourceLink] [nvarchar](400) NOT NULL,
	[RequestCount] [int] NOT NULL,
	[RegDate] [date] NOT NULL,
	[LastAccessDate] [date] NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_MainLinks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MirrorLinkLogs]    Script Date: 02/05/2020 00:36:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MirrorLinkLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MirrorLinkId] [int] NOT NULL,
	[TimeOut] [int] NOT NULL,
	[RegDate] [date] NOT NULL,
 CONSTRAINT [PK_MirrorLinkLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MirrorLinks]    Script Date: 02/05/2020 00:36:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MirrorLinks](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MainLinkId] [int] NOT NULL,
	[ServerId] [int] NOT NULL,
	[TargetLink] [nvarchar](300) NULL,
	[RequestCount] [int] NOT NULL,
	[RegDate] [date] NOT NULL,
	[LastAccessDate] [date] NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_MirrorLinks] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Servers]    Script Date: 02/05/2020 00:36:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IpAddress] [nvarchar](15) NOT NULL,
	[ServerName] [nvarchar](100) NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Servers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[MirrorLinkLogs]  WITH CHECK ADD  CONSTRAINT [FK_MirrorLinkLogs_MirrorLinks] FOREIGN KEY([MirrorLinkId])
REFERENCES [dbo].[MirrorLinks] ([Id])
GO
ALTER TABLE [dbo].[MirrorLinkLogs] CHECK CONSTRAINT [FK_MirrorLinkLogs_MirrorLinks]
GO
ALTER TABLE [dbo].[MirrorLinks]  WITH CHECK ADD  CONSTRAINT [FK_MirrorLinks_MainLinks] FOREIGN KEY([MainLinkId])
REFERENCES [dbo].[MainLinks] ([Id])
GO
ALTER TABLE [dbo].[MirrorLinks] CHECK CONSTRAINT [FK_MirrorLinks_MainLinks]
GO
ALTER TABLE [dbo].[MirrorLinks]  WITH CHECK ADD  CONSTRAINT [FK_MirrorLinks_Servers] FOREIGN KEY([ServerId])
REFERENCES [dbo].[Servers] ([Id])
GO
ALTER TABLE [dbo].[MirrorLinks] CHECK CONSTRAINT [FK_MirrorLinks_Servers]
GO
USE [master]
GO
ALTER DATABASE [DbDownloadManagerSite] SET  READ_WRITE 
GO
